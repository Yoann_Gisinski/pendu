import random, os, platform

# OS Detection
if platform.system() == "Windows":
    clear = "cls"
else:
    clear = "clear"

# Variables
inputs = [] # Liste des lettres saisies
list = [] # Liste de mots disponibles
word_hide = [] # Liste contenant le mot caché
word_show = [] # Liste contenant le mot
points = 11 # Nombre de points restants
run = True # Variable de jeu

# Lecture du fichier sans mots avec espace en minuscule
file = open("liste_francais.txt", "r")
for i in file:
    if not " " in i:
        i = i.strip("\n")
        list.append(i.lower())
file.close()

# Choix aléatoire du mot
word = list[random.randint(0, len(list))]

# Initialisation du mot caché et visible
for i in range(len(word)):
    word_hide.append("-")
    word_show.append(word[i])

# Saisie utilisateur
def tape():
    user_input = str(input("Entrez une lettre ou un mot : "))
    while user_input in inputs or user_input == "":
        if user_input in inputs:
            user_input = str(input("Vous avez déja entré cette lettre. Entrez une autre lettre : "))
        elif user_input == "":
            user_input = str(input("Veuillez entrer une lettre ou un mot : "))
    return user_input.lower()

# Boucle de jeu
while run:
    # Affichage
    os.system(clear)
    print(f"Points : {points}")
    
    # Update camouflage du mot
    for i in range(len(word)):
        print(word_hide[i], end = " ") # Affichage du mot caché
    print()
    
    # Saisie utilisateur
    user_input = tape()
    
    # Vérification saisie
    if len(user_input) == 1: # 1 caractere
        inputs.append(user_input)
        if user_input in word: # Juste
            for i in range(len(word)):
                if word[i] == user_input:
                    word_hide[i] = user_input
        else: # Faux
            points -= 1
            
    else: # 1 mot
        if user_input != word: # Faux
            points -= 2
    
    # Mot trouvé
    if user_input == word or word_hide == word_show:
        run = False
        print(f"Gagné ! Le mot était : {word}")
    
    # Vérification nombre de points
    if points <= 0:
        run = False
        print(f"Perdu ! Le mot était : {word}")